
The docker compose launch 4 images, tztraffic, two nodes, and a baker.

The baker source the baking keys here:

    - $PWD/yes-wallet/public_keys:/var/run/tezos/client/public_keys
    - $PWD/yes-wallet/secret_keys:/var/run/tezos/client/secret_keys
    - $PWD/yes-wallet/public_key_hashs:/var/run/tezos/client/public_key_hashs

The nodes bootstrap from a snapshot :

    - $PWD/tezos.snapshot:/snapshot:ro
