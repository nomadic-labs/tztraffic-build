ARG IMAGE
# hadolint ignore=DL3006
FROM ${IMAGE}

USER root
# hadolint ignore=DL3018
RUN apk --no-cache add curl jq coreutils
USER tezos

COPY --chmod=755 tezos-baker-entrypoint.sh /

ENTRYPOINT ["/tezos-baker-entrypoint.sh"]
