ARG IMAGE
# hadolint ignore=DL3006
FROM ${IMAGE}

USER root
# hadolint ignore=DL3018
RUN apk --no-cache add curl
USER tezos

COPY --chmod=0755 tezos-node-entrypoint.sh /

ENTRYPOINT ["/tezos-node-entrypoint.sh"]

