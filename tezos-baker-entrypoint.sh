#!/bin/sh

set -eu

export NODE_HOST="${NODE_HOST:?}"
export NODE_RPC_PORT="${NODE_RPC_PORT:?}"
export PROTOCOL="${PROTOCOL:?}"
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y

/usr/local/bin/octez-client \
  --base-dir /var/run/tezos/client \
  --endpoint "http://${NODE_HOST}:${NODE_RPC_PORT}" \
  bootstrapped

# bake for until almost bootstrapped
head_timestamp=$(/usr/local/bin/octez-client \
  --base-dir /var/run/tezos/client \
  --endpoint "http://${NODE_HOST}:${NODE_RPC_PORT}" \
  rpc get /chains/main/blocks/head/header \
    | jq .timestamp | tr -d '"')
head_date=$(date --date "${head_timestamp}" '+%s' --utc)
now=$(date '+%s' --utc)
while true; do
    diff=$((now - head_date))
    if [ "$diff" -gt "600" ]; then
        /usr/local/bin/octez-client \
  --base-dir /var/run/tezos/client \
  --endpoint "http://${NODE_HOST}:${NODE_RPC_PORT}" \
  bake for --minimal-timestamp;
        sleep 15;
    else
        break;
    fi
done

sh /usr/local/bin/entrypoint.sh octez-baker \
  --liquidity-baking-toggle-vote pass
