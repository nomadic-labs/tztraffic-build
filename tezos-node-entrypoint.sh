#!/bin/sh

set -e

export NODE_HOST="${NODE_HOST:?}"
export NODE_RPC_PORT="${NODE_RPC_PORT:?}"
export PROTOCOL="${PROTOCOL:?}"
export RESET_NODE="${RESET_NODE:-false}"

if [ -n "$DATADIR_URL" ]; then
  echo "###################### DATADIR DOWNLOAD ! ######################"
  curl -L "$DATADIR_URL" --progress-bar -o /tmp/datadir.tar.gz
  cd /tezos/data || tar -zxvf /tmp/datadir.tar.gz -C node --strip-components 1
  rm -f /tmp/datadir.tar.gz
  echo "###################### DATADIR DOWNLOADED ! ######################"
fi

if [ -n "$INIT_DATADIR_ONLY" ]
then
  echo "waiting for K8 to finish datadir copy"
  sleep 1200
fi

if [ "$RESET_NODE" = "false" ]; then
  if [ -d "/var/run/tezos/node/data/store" ]; then
    echo "data dir initialized. skiping snapshot if provided"
  elif [ -f "/snapshot" ]; then
    rm -Rf /var/run/tezos/node/data
    echo "###################### SNAPSHOT FOUND ! ######################"
    /usr/local/bin/octez-node snapshot import /snapshot --no-check --data-dir "/var/run/tezos/node/data/"
    /usr/local/bin/entrypoint.sh octez-upgrade-storage
  else
    echo "Cannot initialize node. missing snapshot"
    exit 1
  fi
elif [ -f "/snapshot" ]; then
    rm -Rf /var/run/tezos/node/data
    echo "###################### SNAPSHOT FOUND ! ######################"
    /usr/local/bin/octez-node snapshot import /snapshot --no-check --data-dir "/var/run/tezos/node/data/"
    /usr/local/bin/entrypoint.sh octez-upgrade-storage
  else
    echo "Cannot re-initialize node. missing snapshot"
    exit 1
fi

echo "###################### NODE RUN  ######################"
# shellcheck disable=SC2068
/usr/local/bin/entrypoint.sh octez-node $@
